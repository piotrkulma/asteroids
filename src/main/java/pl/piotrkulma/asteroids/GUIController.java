package pl.piotrkulma.asteroids;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class GUIController {
    private GraphicsContext graphicsContext;
    private ActionKeyState actionKeyState;
    private GameEngine gameEngine;

    @FXML
    private Canvas canvas;

    @FXML
    protected void initialize() throws Exception {
        actionKeyState = new ActionKeyState();

        graphicsContext = canvas.getGraphicsContext2D();
        canvas.setFocusTraversable(true);

        initAnimation();
    }

    @FXML
    protected void canvasOnKeyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case UP:
                actionKeyState.setUpPressed(false);
                break;
            case LEFT:
                actionKeyState.setLeftPressed(false);
                break;
            case RIGHT:
                actionKeyState.setRightPressed(false);
                break;
            case SPACE:
                actionKeyState.setSpacePressed(false);
                break;
            case R:
                actionKeyState.setResetPressed(false);
                break;
            default:
                break;
        }

        keyEvent.consume();
    }

    @FXML
    protected void canvasOnKeyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case UP:
                actionKeyState.setUpPressed(true);
                break;
            case LEFT:
                actionKeyState.setLeftPressed(true);
                break;
            case RIGHT:
                actionKeyState.setRightPressed(true);
                break;
            case SPACE:
                actionKeyState.setSpacePressed(true);
                break;
            case R:
                actionKeyState.setResetPressed(true);
                break;

            default:
                break;
        }

        keyEvent.consume();
    }

    private void initAnimation() {
        gameEngine = new GameEngine(graphicsContext, actionKeyState);

        final Timeline animation = new Timeline(
                new KeyFrame(Duration.seconds(0.01),
                        actionEvent -> gameEngine.drawSingleFrame())
        );

        animation.setCycleCount(Animation.INDEFINITE);
        animation.play();
    }
}
