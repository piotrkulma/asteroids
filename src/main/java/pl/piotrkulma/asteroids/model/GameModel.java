package pl.piotrkulma.asteroids.model;

import pl.piotrkulma.asteroids.objects.gameObjects.GameObject;
import pl.piotrkulma.asteroids.objects.gameObjects.Player;
import pl.piotrkulma.asteroids.utils.GameObjectUtils;

import java.util.List;

/**
 * Created by Piotr Kulma on 22.07.2017.
 */
public final class GameModel {
    private int score;
    private Player player;
    private List<GameObject> gameObjects;

    public GameModel(List<GameObject> gameObjects) {
        this.gameObjects = gameObjects;
        this.player = GameObjectUtils.findPlayer(this.gameObjects);

        this.score = 0;
    }

    public void increaseScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public Player getPlayer() {
        return player;
    }

    public List<GameObject> getGameObjects() {
        return gameObjects;
    }
}
