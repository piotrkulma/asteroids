package pl.piotrkulma.asteroids;

import javafx.scene.canvas.GraphicsContext;
import org.apache.log4j.Logger;
import pl.piotrkulma.asteroids.model.GameModel;
import pl.piotrkulma.asteroids.objects.gameObjects.*;
import pl.piotrkulma.asteroids.utils.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Created by Piotr Kulma on 27.06.2017.
 */
public class GameEngine {
    private final static Logger LOG = Logger.getLogger(GameEngine.class);

    private double[] playerVectorBeforeMove;

    private ActionKeyState actionKeyState;
    private GraphicsContext gc;
    private GameModel gameModel;
    private List<GameObject> newObjectsTemp;

    public GameEngine(GraphicsContext gc, ActionKeyState actionKeyState) {
        this.gc = gc;
        this.gameModel = new GameModel(TemplateUtils.getInitialGameObjects());
        this.actionKeyState = actionKeyState;

        newObjectsTemp = new ArrayList<>();
    }

    public void drawSingleFrame() {
        gc.clearRect(0, 0, GameConsts.CANVAS_WIDTH, GameConsts.CANVAS_HEIGHT);
        gc.strokeText("SCORE " + gameModel.getScore(), 410, 20);

        if(gameModel.getPlayer().getEnergy() == 0d) {
            playerVectorBeforeMove = gameModel.getPlayer().getV();
        }

        onKeyAction();

        Iterator<GameObject> iterator = gameModel.getGameObjects().iterator();

        while(iterator.hasNext()) {
            GameObject object = iterator.next();

            GraphicUtils.drawGameObject(gc, object);
            GameObjectUtils.moveGameObject(object);

            checkIfBulletCollidingAsteroid(object);

            checkIfObjectBehindTheScene(object);
        }

        GameObjectUtils.removeInactiveObjects(gameModel.getGameObjects());

        GameObjectUtils.movePlayer(gameModel.getPlayer(), playerVectorBeforeMove);

        GameObjectUtils.copyNewObjects(gameModel.getGameObjects(), newObjectsTemp);
    }

    private void checkIfBulletCollidingAsteroid(GameObject asteroidObj) {
        Optional<List<GameObject>> splitterParts = CollisionUtils.checkIfBulletCollidingAsteroid(gameModel, asteroidObj);
        if(splitterParts.isPresent()) {
            gameModel.increaseScore();
            newObjectsTemp.addAll(splitterParts.get());
        }
    }

    private void checkIfObjectBehindTheScene(GameObject object) {
        if(GameObjectUtils.isBehindTheScene(object)) {
            if (GameObjectUtils.isBullet(object)|| GameObjectUtils.isDust(object)) {
                object.setGameObjectState(GameObjectState.INACTIVE);
            } else {
                GameObjectUtils.moveBackToScene(object);
            }
        }
    }

    private void onKeyAction() {
        if(actionKeyState.isUpPressed()) {
            if(playerVectorBeforeMove[0] == gameModel.getPlayer().getV()[0] && playerVectorBeforeMove[1] == gameModel.getPlayer().getV()[1]) {
                gameModel.getPlayer().increaseEngineEnergy();
           }else {
                gameModel.getPlayer().decreaseEngineEnergy(0.05);
            }
        }

        if(!actionKeyState.isUpPressed()) {
            gameModel.getPlayer().decreaseEngineEnergy(0.009);
        }

        if(actionKeyState.isLeftPressed()) {
            GameObjectUtils.rotateObject(gameModel.getPlayer(), -2);
        }

        if(actionKeyState.isRightPressed()) {
            GameObjectUtils.rotateObject(gameModel.getPlayer(), 2);
        }

        if(actionKeyState.isSpacePressed()) {
            gameModel.getGameObjects().add(TemplateUtils.getBulletTemplate(gameModel.getPlayer()));
            actionKeyState.setSpacePressed(false);
        }

        if(actionKeyState.isResetPressed()) {
            actionKeyState.setResetPressed(false);

            gameModel = new GameModel(TemplateUtils.getInitialGameObjects());
        }

        if(actionKeyState.isUpPressed() && (actionKeyState.isLeftPressed() || actionKeyState.isRightPressed())) {
            LOG.info("BOTH");
        }
    }
}
