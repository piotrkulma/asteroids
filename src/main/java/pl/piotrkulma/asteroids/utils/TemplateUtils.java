package pl.piotrkulma.asteroids.utils;

import pl.piotrkulma.asteroids.objects.gameObjects.*;
import pl.piotrkulma.asteroids.objects.basic2d.Point2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Piotr Kulma on 14.07.2017.
 */
public final class TemplateUtils {
    private TemplateUtils() {
    }

    public static List<GameObject> getInitialGameObjects() {
        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());
        gameObjects.add(TemplateUtils.getAsteroidTemplate());

        gameObjects.add(TemplateUtils.getPlayerTemplate(10));

        return gameObjects;
    }

    public static Bullet getBulletTemplate(Player player) {
        Bullet bullet = new Bullet(player.getPx(), player.getPy(), player.getV());

        bullet.addPoint(new Point2d(-1d, 1d));
        bullet.addPoint(new Point2d(1d, 1d));
        bullet.addPoint(new Point2d(1d, -1d));
        bullet.addPoint(new Point2d(-1d, -1d));

        return bullet;
    }

    public static Dust getDustTemplate(double px, double py, double[] v) {
        Dust dust = new Dust(px, py, v);

        dust.addPoint(new Point2d(-1d, 1d));
        dust.addPoint(new Point2d(1d, 1d));
        dust.addPoint(new Point2d(1d, -1d));
        dust.addPoint(new Point2d(-1d, -1d));

        return dust;
    }

    public static Player getPlayerTemplate(double scale) {
        Player player = new Player(300, 300, scale, new double[]{0d, 1d});
        player.addPoint(new Point2d(0d, 1d));
        player.addPoint(new Point2d(1d, -1d));
        player.addPoint(new Point2d(-1d, -1d));

        return player;
    }

    public static Asteroid getAsteroidTemplate(double px, double py, double scale, double rotation, double v[]) {
        Random random = new Random();
        random.setSeed(System.nanoTime());
        int template = (int) (random.nextDouble() * 2d);

        if(template == 0) {
            return getAsteroidTemplate01(px, py, scale, rotation, v);
        } else {
            return getAsteroidTemplate02(px, py, scale, rotation, v);
        }
    }

    public static Asteroid getAsteroidTemplate() {
        Random r = new Random();
        r.setSeed(System.nanoTime());

        double scale = (r.nextDouble()) * 15d;
        if(scale < GameConsts.ASTEROID_MINIMUM_SCALE) {
            scale = GameConsts.ASTEROID_MINIMUM_SCALE;
        }

        return getAsteroidTemplate(r.nextDouble() * GameConsts.CANVAS_WIDTH,
                r.nextDouble() * GameConsts.CANVAS_HEIGHT,
                scale,
                r.nextDouble() * nextSign(r),
                nextV());
    }

    public static double[] nextV() {
        Random r = new Random();
        r.setSeed(System.nanoTime());
        return new double[]{r.nextDouble() * nextSign(r) * r.nextDouble(), r.nextDouble() * nextSign(r) * r.nextDouble()};
    }

    private static double nextSign(Random random) {
        return random.nextBoolean()?-1:1;
    }

    private static Asteroid getAsteroidTemplate01(double px, double py, double scale, double rotation, double v[]) {
        Asteroid asteroid = new Asteroid(px, py, scale, rotation, v);

        asteroid.addPoint(new Point2d(-5, 3));
        asteroid.addPoint(new Point2d(-4, 5));
        asteroid.addPoint(new Point2d(-2, 6));
        asteroid.addPoint(new Point2d(0, 5));
        asteroid.addPoint(new Point2d(2, 4));
        asteroid.addPoint(new Point2d(3, 1));
        asteroid.addPoint(new Point2d(2, 1));
        asteroid.addPoint(new Point2d(1, -1));
        asteroid.addPoint(new Point2d(-2, -2));
        asteroid.addPoint(new Point2d(-3, 1));


        return asteroid;
    }

    private static Asteroid getAsteroidTemplate02(double px, double py, double scale, double rotation, double v[]) {
        Asteroid asteroid = new Asteroid(px, py, scale, rotation, v);

        asteroid.addPoint(new Point2d(-4, 3));
        asteroid.addPoint(new Point2d(-2, 5));
        asteroid.addPoint(new Point2d(1, 5));
        asteroid.addPoint(new Point2d(2, 2));
        asteroid.addPoint(new Point2d(4, 3));
        asteroid.addPoint(new Point2d(3, -3));
        asteroid.addPoint(new Point2d(1, -2));
        asteroid.addPoint(new Point2d(-2, -3));
        asteroid.addPoint(new Point2d(-5, -1));

        return asteroid;
    }
}
