package pl.piotrkulma.asteroids.utils;

import javafx.scene.canvas.GraphicsContext;
import pl.piotrkulma.asteroids.objects.basic2d.Point2d;
import pl.piotrkulma.asteroids.objects.gameObjects.Asteroid;
import pl.piotrkulma.asteroids.objects.gameObjects.GameObject;
import pl.piotrkulma.asteroids.objects.gameObjects.Player;

import java.util.Iterator;

/**
 * Created by Piotr Kulma on 15.07.2017.
 */
public final class GraphicUtils {
    private GraphicUtils() {
    }

    public static void drawGameObject(GraphicsContext gc, GameObject gameObject) {
        Iterator<Point2d> iterator = gameObject.getPoints().iterator();

        Point2d act;
        Point2d prev = null;

        double px = gameObject.getPx();
        double py = gameObject.getPy();

        while (iterator.hasNext()) {
            act = iterator.next();

            if(prev != null) {
                gc.strokeLine(prev.getX() + px, prev.getY() + py, act.getX() + px, act.getY() + py);
                if(!iterator.hasNext()) {
                    prev = gameObject.getPoints().get(0);
                    gc.strokeLine(prev.getX() + px, prev.getY() + py, act.getX() + px, act.getY() + py);

                    if(GameConsts.DEBUG_MODE) {
                        if (GameObjectUtils.isAsteroid(gameObject)) {
                            gc.strokeText(Double.toString(gameObject.getScale()), prev.getX() + px, prev.getY() + py);
                        } else if (GameObjectUtils.isPlayer(gameObject)) {
                            gc.strokeText(Double.toString(((Player) gameObject).getEnergy()), prev.getX() + px, prev.getY() + py);
                        }
                    }
                }
            }

            prev = act;
        }
    }

}
