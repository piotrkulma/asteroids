package pl.piotrkulma.asteroids.utils;

import pl.piotrkulma.asteroids.objects.basic2d.Point2d;
import pl.piotrkulma.asteroids.objects.gameObjects.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * Created by Piotr Kulma on 01.07.2017.
 */
public final class GameObjectUtils {
    private static double PART_BASE[] = {5d, 4d, 3d, 2d, 2d, 2d, 2d,  1d, 1d, 1d};

    private GameObjectUtils() {
    }

    public static boolean isNotPlayer(GameObject go) {
        return !isPlayer(go);
    }

    public static boolean isNotAsteroid(GameObject go) {
        return !isAsteroid(go);
    }

    public static boolean isNotDust(GameObject go) {
        return !isDust(go);
    }

    public static boolean isNotBullet(GameObject go) {
        return !isNotBullet(go);
    }

    public static boolean isPlayer(GameObject go) {
        return go instanceof Player;
    }

    public static boolean isAsteroid(GameObject go) {
        return go instanceof Asteroid;
    }

    public static boolean isDust(GameObject go) {
        return go instanceof Dust;
    }

    public static boolean isBullet(GameObject go) {
        return go instanceof Bullet;
    }

    public static void moveGameObject(GameObject gameObject) {
        if(isPlayer(gameObject)) {
            return;
        }

        if(isAsteroid(gameObject)) {
            Asteroid asteroid = (Asteroid) gameObject;
            GameObjectUtils.rotateObject(asteroid, asteroid.getRotation());
        }

        if(isDust(gameObject)) {
            Dust dust = (Dust) gameObject;
            dust.decreaseLifetime();
        }

        gameObject.setPx(gameObject.getPx() + gameObject.getV()[0]);
        gameObject.setPy(gameObject.getPy() + gameObject.getV()[1]);
    }

    public static void rotateObject(GameObject gameObject, double angle) {
        double p[];

        Point2d act;
        Iterator<Point2d> iterator = gameObject.getPoints().iterator();

        while (iterator.hasNext()) {
            act = iterator.next();
            p = MathUtils.rotate(act, angle);

            act.setX(p[0]);
            act.setY(p[1]);
        }

        if(isPlayer(gameObject)) {
            gameObject.setV(MathUtils.rotate(gameObject.getV()[0], gameObject.getV()[1], angle));
        }
    }

    public static void movePlayer(Player player, double[] playerVectorBeforeMove) {
        player.setPx(player.getPx() + playerVectorBeforeMove[0] * player.getEnergy());
        player.setPy(player.getPy() + playerVectorBeforeMove[1] * player.getEnergy());
    }

    public static void moveBackToScene(GameObject gameObject) {
        if(gameObject.getPx() > GameConsts.CANVAS_WIDTH) {
            gameObject.setPx(0);
        } else if(gameObject.getPx() < 0) {
            gameObject.setPx(GameConsts.CANVAS_WIDTH);
        }

        if(gameObject.getPy() > GameConsts.CANVAS_HEIGHT) {
            gameObject.setPy(0);
        } else if(gameObject.getPy() < 0) {
            gameObject.setPy(GameConsts.CANVAS_HEIGHT);
        }
    }

    public static void removeInactiveObjects(List<GameObject> gameObjects) {
        Iterator<GameObject> gameObjectIterator = gameObjects.iterator();

        while(gameObjectIterator.hasNext()) {
            GameObject gameObject = gameObjectIterator.next();

            if(gameObject.getGameObjectState() != GameObjectState.ACTIVE) {
                gameObjectIterator.remove();
            }
        }
    }

    public static boolean isBehindTheScene(GameObject gameObject) {
        for(Point2d point2d : gameObject.getPoints()) {
            if (((gameObject.getPx() + point2d.getX() >= 0 && gameObject.getPx() + point2d.getX() <= GameConsts.CANVAS_WIDTH)
                    && (gameObject.getPy() + point2d.getY() >= 0 && gameObject.getPy() + point2d.getY() <= GameConsts.CANVAS_HEIGHT))) {
                return false;
            }
        }

        return true;
    }

    public static Player findPlayer(List<GameObject> gameObjects) {
        for(GameObject gameObject : gameObjects) {
            if(isPlayer(gameObject)) {
                return (Player)gameObject;
            }
        }

        return null;
    }

    public static List<Bullet> findBullets(List<GameObject> gameObjects) {
        List<Bullet> bullets = new ArrayList<>();

        for(GameObject gameObject : gameObjects) {
            if(isBullet(gameObject)) {
                bullets.add((Bullet) gameObject);
            }
        }

        return bullets;
    }

    public static Optional<List<Double>> calculateSplittedParts(double size) {
        if(size <= GameConsts.ASTEROID_MINIMUM_SCALE_SPLITTED) {
            return Optional.empty();
        }

        int index = 0;
        double tempSize = size;

        List<Double> parts = new ArrayList<>();
        while(tempSize > 0.99) {
            double part = PART_BASE[index];

            if((parts.size() > 0 && tempSize >= part) || (parts.size() == 0 && tempSize > part)) {
                tempSize -= part;
                parts.add(part);
            } else {
                index++;
            }
        }

        return Optional.of(parts);
    }

    public static List<GameObject> getSplittedAsteroidParts(Asteroid a) {
        List<GameObject> splitterParts = new ArrayList<>();
        Optional<List<Double>> partsScaleOptional = GameObjectUtils.calculateSplittedParts(a.getScale());

        if (partsScaleOptional.isPresent()) {
            partsScaleOptional.get().forEach(
                    scale ->
                            splitterParts.add(
                                    TemplateUtils.getAsteroidTemplate(
                                            a.getPx(), a.getPy(), scale, a.getRotation(), TemplateUtils.nextV()))
            );
        }

        return splitterParts;
    }

    public static List<GameObject> getDustsParts(Asteroid a) {
        List<GameObject> splitterParts = new ArrayList<>();

        a.getScale();
        IntStream.range(1, 5 * (int)a.getScale()).forEach(
                i -> {
                    splitterParts.add(TemplateUtils.getDustTemplate(a.getPx(), a.getPy(), TemplateUtils.nextV()));
                }
        );

        return splitterParts;
    }

    public static <T extends GameObject> void copyNewObjects(List<GameObject> mainObjectList, List<T> newObjects) {
        if(newObjects.size() > 0) {
            mainObjectList.addAll(newObjects);
            newObjects.clear();
        }
    }
}
