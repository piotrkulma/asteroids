package pl.piotrkulma.asteroids.utils;

import pl.piotrkulma.asteroids.objects.basic2d.Point2d;

/**
 * Created by Piotr Kulma on 01.07.2017.
 */
public final class MathUtils {
    private MathUtils() {
    }

    public static double degToRad(double degrees) {
        return degrees * (Math.PI / 180d);
    }

    public static double[] rotate(double x, double y, double angle) {
        double rad = degToRad(angle);
        return new double[] {
                (x * Math.cos(rad)) - (y * Math.sin(rad)),
                (y * Math.cos(rad)) + (x * Math.sin(rad))
        };
    }

    public static double[] rotate(Point2d point2d, double angle) {
        return rotate(point2d.getX(), point2d.getY(), angle);
    }
}
