package pl.piotrkulma.asteroids.utils;

import pl.piotrkulma.asteroids.model.GameModel;
import pl.piotrkulma.asteroids.objects.basic2d.Point2d;
import pl.piotrkulma.asteroids.objects.gameObjects.Asteroid;
import pl.piotrkulma.asteroids.objects.gameObjects.Bullet;
import pl.piotrkulma.asteroids.objects.gameObjects.GameObject;
import pl.piotrkulma.asteroids.objects.gameObjects.GameObjectState;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Piotr on 15.07.2017.
 */
public final class CollisionUtils {
    private CollisionUtils() {
    }

    public static Optional<List<GameObject>> checkIfBulletCollidingAsteroid(GameModel gameModel, GameObject asteroidObj) {
        if(GameObjectUtils.isNotAsteroid(asteroidObj)) {
            return Optional.empty();
        }

        Asteroid asteroid = (Asteroid) asteroidObj;

        List<GameObject> splitterParts = new ArrayList<>();

        GameObjectUtils.findBullets(gameModel.getGameObjects()).stream().forEach(
                bullet -> {
                    if (CollisionUtils.isBulletCollidingAsteroid(asteroid, bullet)) {
                        bullet.setGameObjectState(GameObjectState.INACTIVE);
                        asteroid.setGameObjectState(GameObjectState.INACTIVE);

                        splitterParts.addAll(GameObjectUtils.getSplittedAsteroidParts(asteroid));
                        splitterParts.addAll(GameObjectUtils.getDustsParts(asteroid));
                    }
                }
        );

        return splitterParts.size()==0?Optional.empty():Optional.of(splitterParts);
    }

    public static boolean isBulletCollidingAsteroid(Asteroid asteroid, Bullet bullet) {
        Point2d collisionPoint =
                new Point2d(
                        bullet.getPoints().get(0).getX() + bullet.getPx(),
                        bullet.getPoints().get(0).getY() + bullet.getPy());

        return isPointInsideObject(asteroid, collisionPoint);
    }

    /**
     * https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
     *
     * @param go
     * @param point2d
     * @return
     */
    private static boolean isPointInsideObject(GameObject go, Point2d point2d) {
        int i;
        int j;
        boolean result = false;

        double testx = point2d.getX();
        double testy = point2d.getY();

        for (i = 0, j = go.getPoints().size() - 1; i < go.getPoints().size(); j = i++) {
            if ( ((verty(go, i)>testy) != (verty(go, j)>testy)) &&
                    (testx < (vertx(go, j)-vertx(go, i)) * (testy-verty(go, i)) / (verty(go, j)-verty(go, i)) + vertx(go, i)) ) {
                result = !result;
            }
        }
        return result;
    }

    private static double vertx(GameObject gameObject, int index) {
        return gameObject.getPoints().get(index).getX() + gameObject.getPx();
    }

    private static double verty(GameObject gameObject, int index) {
        return gameObject.getPoints().get(index).getY() + gameObject.getPy();
    }
}
