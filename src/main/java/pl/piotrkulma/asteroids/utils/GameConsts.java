package pl.piotrkulma.asteroids.utils;

/**
 * Created by Piotr Kulma on 15.07.2017.
 */
public final class GameConsts {
    public static final double ASTEROID_MINIMUM_SCALE = 3.0d;
    public static final double ASTEROID_MINIMUM_SCALE_SPLITTED = 1.0d;

    public static final double CANVAS_WIDTH = 500d;
    public static final double CANVAS_HEIGHT = 500d;

    public static final boolean DEBUG_MODE = false;

    private GameConsts() {
    }
}
