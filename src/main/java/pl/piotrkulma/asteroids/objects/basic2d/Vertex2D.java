package pl.piotrkulma.asteroids.objects.basic2d;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr Kulma on 01.07.2017.
 */
public class Vertex2D {
    protected double px;
    protected double py;
    protected double scale;

    private List<Point2d> points;

    public Vertex2D(double px, double py, double scale) {
        this.px = px;
        this.py = py;
        this.scale = scale;
        points = new ArrayList<>();
    }

    public void addPoint(Point2d point2d) {
        point2d.setX(point2d.getX() * scale);
        point2d.setY(point2d.getY() * scale);
        points.add(point2d);
    }

    public List<Point2d> getPoints() {
        return points;
    }

    public double getPx() {
        return px;
    }

    public void setPx(double px) {
        this.px = px;
    }

    public double getPy() {
        return py;
    }

    public void setPy(double py) {
        this.py = py;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Vertex2D{" +
                "px=" + px +
                ", py=" + py +
                ", points=" + points +
                '}';
    }
}
