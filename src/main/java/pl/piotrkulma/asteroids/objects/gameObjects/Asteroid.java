package pl.piotrkulma.asteroids.objects.gameObjects;

/**
 * Created by Piotr Kulma on 27.06.2017.
 */
public class Asteroid extends GameObject {
    private double rotation;

    public Asteroid(double px, double py, double scale, double rotation, double[] v) {
        super(px, py, scale, v);

        this.rotation = rotation;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }
}
