package pl.piotrkulma.asteroids.objects.gameObjects;

/**
 * Created by Piotr Kulma on 15.07.2017.
 */
public enum GameObjectState {
    ACTIVE,
    INACTIVE
}
