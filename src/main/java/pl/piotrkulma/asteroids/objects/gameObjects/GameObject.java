package pl.piotrkulma.asteroids.objects.gameObjects;

import pl.piotrkulma.asteroids.objects.basic2d.Vertex2D;

import java.util.Arrays;

/**
 * Created by Piotr Kulma on 14.07.2017.
 */
public class GameObject extends Vertex2D {
    private GameObjectState gameObjectState;
    protected double v[];

    public GameObject(double px, double py, double scale, double v[]) {
        super(px, py, scale);
        this.gameObjectState = GameObjectState.ACTIVE;
        this.v = v;
    }

    public double[] getV() {
        return Arrays.copyOf(v, v.length);
    }

    public void setV(double[] v) {
        this.v = v;
    }

    public GameObjectState getGameObjectState() {
        return gameObjectState;
    }

    public void setGameObjectState(GameObjectState gameObjectState) {
        this.gameObjectState = gameObjectState;
    }
}
