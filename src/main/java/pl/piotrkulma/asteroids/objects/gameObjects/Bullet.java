package pl.piotrkulma.asteroids.objects.gameObjects;

/**
 * Created by Piotr Kulma on 14.07.2017.
 */
public class Bullet extends GameObject {
    private static final double BULLET_SCALE = 2.0d;
    private static final double BULLET_SPEED_MULTIPLIER = 3.0d;

    public Bullet(double px, double py, double[] v) {
        super(px, py, BULLET_SCALE, v);

        this.v[0] = v[0] * BULLET_SPEED_MULTIPLIER;
        this.v[1] = v[1] * BULLET_SPEED_MULTIPLIER;
    }
}
