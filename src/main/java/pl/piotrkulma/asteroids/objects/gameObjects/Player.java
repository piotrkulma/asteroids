package pl.piotrkulma.asteroids.objects.gameObjects;

/**
 * Created by Piotr Kulma on 01.07.2017.
 */
public class Player extends GameObject {
    private double energy;
    public Player(double px, double py, double scale, double v[]) {
        super(px, py, scale, v);

        this.energy = 0d;
    }

    public double getEnergy() {
        return energy;
    }

    public void increaseEngineEnergy() {
        if (energy < 3.0) {
            energy += 0.1;
        }
    }

    public void decreaseEngineEnergy(double value) {
        if(energy > 0) {
            energy -= value;
        } else {
            energy = 0d;
        }
    }
}
