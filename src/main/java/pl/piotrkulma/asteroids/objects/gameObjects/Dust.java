package pl.piotrkulma.asteroids.objects.gameObjects;

import java.util.Random;

/**
 * Created by Piotr Kulma on 22.07.2017.
 */
public class Dust extends GameObject {
    private static final double DUST_SCALE = 0.5d;
    private static final double DUST_SPEED_MULTIPLIER = 3.0d;

    private double lifetime;

    public Dust(double px, double py, double[] v) {
        super(px, py, DUST_SCALE, v);

        this.v[0] = v[0] * DUST_SPEED_MULTIPLIER;
        this.v[1] = v[1] * DUST_SPEED_MULTIPLIER;

        initLifetime();
    }

    public void decreaseLifetime() {
        this.lifetime--;
    }

    @Override
    public GameObjectState getGameObjectState() {
        if(lifetime > 0d) {
            return GameObjectState.ACTIVE;
        }

        return GameObjectState.INACTIVE;
    }

    private void initLifetime() {
        Random r = new Random();
        r.setSeed(System.nanoTime());

        this.lifetime = (r.nextDouble() * 100) + 50;
    }
}
